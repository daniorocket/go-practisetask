package main

import (
	"fmt"
)

//Task 1
func repeatString(word string, counter int) string {
	if counter <= 0 {
		return ""
	}
	fmt.Print(word)
	return repeatString(word, counter-1)
}

//Task 2
func findElementsByLength(arr []string, requiredLength int) {
	if requiredLength < 0 {
		fmt.Println("Length as argument can't be less than 0!")
		return
	}
	if len(arr) == 0 || cap(arr) == 0 {
		fmt.Println("Passed data can't be empty!")
		return
	}
	isFirstElement := true
	for i := 0; i < len(arr); i++ {
		if len(arr[i]) > requiredLength {
			if isFirstElement == true {
				fmt.Print(arr[i])
				isFirstElement = false
			} else {
				fmt.Print(", ", arr[i])
			}
		}
	}
	if isFirstElement == true {
		fmt.Println("In array ", arr, "can't find elements longer than", requiredLength, ".")
	}
}

//Task 3
func reverseText(text string) string {
	var reversedText string
	for i := len(text) - 1; i >= 0; i-- {
		reversedText += string(text[i])
	}
	return reversedText
}

//Task 4

//This struct describe single Product.
type Product struct {
	price    float64
	name     string
	category string
}

//This const describe minimal price of product
const MinPrice = 50

func findProductsByPrice(listOfProducts []Product) {
	isAnyElement := false
	fmt.Printf("List of products worth more than %d.\n", MinPrice)
	for _, product := range listOfProducts {
		if product.price > MinPrice {
			fmt.Printf("Name: %s Category: %s Price %.2f\n", product.name, product.category, product.price)
			isAnyElement = true
		}
	}
	if !isAnyElement {
		fmt.Println("There isn't any products.")
	}
}
func findProductsByCategory(listOfProducts []Product, category string) {
	isAnyElement := false
	fmt.Printf("List of products from category %s.\n", category)
	for _, product := range listOfProducts {
		if product.category == category {
			fmt.Printf("Name: %s Category: %s Price %.2f\n", product.name, product.category, product.price)
			isAnyElement = true
		}
	}
	if !isAnyElement {
		fmt.Println("There isn't any products.")
	}
}

func findProductsByCategoryAndPrice(listOfProducts []Product, category string) {
	isAnyElement := false
	fmt.Printf("List of products from category %s worth more than %d\n", category, MinPrice)
	for _, product := range listOfProducts {
		if product.category == category && product.price > MinPrice {
			fmt.Printf("Name: %s Category: %s Price %.2f\n", product.name, product.category, product.price)
			isAnyElement = true
		}
	}
	if !isAnyElement {
		fmt.Println("There isn't any products.")
	}
}

//Task5

//Abstract Figure
type Figure interface {
	Area() int
}

type myRectangleType struct {
	A int
	B int
}

func printArea(figure Figure) {
	fmt.Println("Pole mojej figury to", figure.Area())
}

func (m myRectangleType) Area() int {
	return m.A * m.B
}

func main() {
	fmt.Println(repeatString("tajne slowo", 3))

	data := []string{
		"a", "bb", "ccc", "dddd", "eeeee", "ffffff", "123321123",
	}
	findElementsByLength(data, 5)
	fmt.Println()
	fmt.Println(reverseText("text"))

	listOfProducts := []Product{
		Product{
			price:    5,
			name:     "Tomato",
			category: "groceries",
		},
		Product{
			price:    3499,
			name:     "Electric mountain Bike",
			category: "sport",
		},
		Product{
			price:    1099,
			name:     "Mountain Bike",
			category: "sport",
		},
		Product{
			price:    7,
			name:     "Orange",
			category: "groceries",
		},
		Product{
			price:    12499,
			name:     "Snooker table",
			category: "sport",
		},
	}
	findProductsByPrice(listOfProducts)
	findProductsByCategory(listOfProducts, "sport")
	findProductsByCategoryAndPrice(listOfProducts, "groceries")

	rectangle := myRectangleType{
		A: 5,
		B: 2,
	}
	printArea(rectangle)
}
